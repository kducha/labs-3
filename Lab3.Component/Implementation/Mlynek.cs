﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Lab3.Contract;

namespace Lab3.Implementation
{
    class Młynek : IMiel
    {
        public void ZmielKawe()
        {
            Console.WriteLine("Mieli");
        }

        public void WylaczMlynek()
        {
            Console.WriteLine("Młynek wylaczony");
        }
    }
}
