﻿using System;
using Lab3.Contract;
using Lab3.Implementation;

namespace Lab3
{
    public struct LabDescriptor
    {
        #region P1

        public static Type I1 = typeof(IFiltruj);
        public static Type I2 = typeof(IMiel);
        public static Type I3 = typeof(IGrzej);

        public static Type Component = typeof(Sterownik);

        public delegate object GetInstance(object Sterownik);

        public static GetInstance GetInstanceOfI1 = (Component) => Component;
        public static GetInstance GetInstanceOfI2 = (Component) => Component;
        public static GetInstance GetInstanceOfI3 = (Component) => Component;
        
        #endregion

        #region P2

        public static Type Mixin = typeof(MixTest);
        public static Type MixinFor = typeof(IGrzej);

        #endregion
    }
}
